import { loadTranslations, locale } from '$lib/i18n';
import type { LayoutLoad } from './$types';

export const load = (async ({ url }) => {
	const { pathname } = url;
	const defaultLocale = 'ja';
	const initLocale = locale.get() || defaultLocale;
	await loadTranslations(initLocale, pathname);

	return {};
}) satisfies LayoutLoad;
