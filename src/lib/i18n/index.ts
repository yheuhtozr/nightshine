import i18n, { type Config } from 'sveltekit-i18n';
import { pl } from './pluralModifier';
import { exist } from './existenceModifier';
import lang from './lang.json';

interface AllowedParams {
	name?: string;
	text?: string;
	reply_to?: string;
	value?: unknown;
	count?: number;
	time?: number;
	duration?: number;
	n_media?: number;
	n_reply?: number | string;
}

const config: Config<AllowedParams> = {
	translations: {
		en: { lang },
		ja: { lang }
	},
	parserOptions: {
		customModifiers: { pl, exist }
	},
	loaders: [
		{
			locale: 'en',
			key: 'main',
			loader: async () => (await import('./en/main.json')).default
		},
		{
			locale: 'en',
			key: 'post',
			loader: async () => (await import('./en/timeline.json')).default
		},
		{
			locale: 'en',
			key: 'composer',
			loader: async () => (await import('./en/composer.json')).default
		},
		{
			locale: 'ja',
			key: 'main',
			loader: async () => (await import('./ja/main.json')).default
		},
		{
			locale: 'ja',
			key: 'post',
			loader: async () => (await import('./ja/timeline.json')).default
		},
		{
			locale: 'ja',
			key: 'composer',
			loader: async () => (await import('./ja/composer.json')).default
		}
	]
};

export const { t, locale, locales, loading, loadTranslations } = new i18n(config);
