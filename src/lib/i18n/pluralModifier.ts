import plural from 'plurals-cldr';
import type { Modifier } from 'sveltekit-i18n';

export const pl: Modifier.T = ({ value, options = [], defaultValue = '', locale }) => {
	const category = plural(locale || 'i-default', value);
	return options.find(({ key }) => key === category)?.value || defaultValue;
};
