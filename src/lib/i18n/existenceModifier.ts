import type { Modifier } from 'sveltekit-i18n';

export const exist: Modifier.T = ({ value, options = [], defaultValue = '' }) => {
	const existence = value != null ? 'y' : 'n';
	return options.find(({ key }) => key === existence)?.value || defaultValue;
};
